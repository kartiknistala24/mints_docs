**Palettes** are the building block underlying architecture for our integration where it makes connection between two applications with standard patterns for an application to connect with another application which takes the data, transforms the data and sends it to another application.
In Administration —> Palettes, Create a new Palette if not exit.
Enter Name of the palette and we need to select the item from the group. 
   *This group contains different actions to perform patterns.
     Input & Outputs
     Orchestration
     Transform
We need to pick the icon from a group of icons.
Down left side find Add icon where we need to add new Palette Properties click on it. 
On opening of the dialog pop-up.  
Fill the Label and Key. Here the key value should not contain any white space. This value will be considered to be key for default configuration.
 Next select any one from given set attributes inside Type properties.
If we select a list attribute it asks us to enter some key value which will be injected to palette properties to Source type configuration.
Assigning a default value will make global default configuration. Initially it was False.
Check if any additional Option is required then make it enabled.
 Finally we need to add condition based configuration if necessary.
Save the property and it is required then add a few more properties as follows.
After adding all the properties save the palette.
