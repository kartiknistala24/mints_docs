# Application Integration

Application integration is the process of enabling independently designed applications to work together. Commonly required capabilities include:

●      Keeping separate copies of data (in independently designed applications) consistent

●      Orchestrating the integrated flow of multiple activities performed by disparate applications

●      Providing access to data and functionality from independently designed applications through what appears to be a single user interface or application service.
