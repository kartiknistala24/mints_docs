# About MINTS&#x20;

**MINTS** is a combination of different types of customised framework for integrating multiple applications.This can be given as a package to the client with proper authentication and authorization. Though we have built this application with Apache Camel, the client may say if I have my own framework but I don't have some extra features like UI or backend support or non functional requirements.At that point, our product should be in helpful in such a way that it can be decoupled into different components and can be plugged into to some other product.
