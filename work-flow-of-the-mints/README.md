# Work Flow of the MINTS

An **Integration** is a sequence of actions that includes tasks like, receives data from an application, processes data & extract information, transforms data/info from one form to another or to a different format, and sends data to different applications with respective formats. There are different applications which perform these actions.

The goal of this UI is to configure these applications to make them seamlessly coordinate among themselves to complete the process. The integration contains all the steps to configure these actions.
