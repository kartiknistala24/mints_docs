# Table of contents

* [README](README.md)
* [MINTS](mDocs.md)
* [Application Integration](application-integration.md)
* [About MINTS ](about-mints.md)
* [Work Flow of the MINTS](work-flow-of-the-mints/README.md)
  * [Application Configuration](work-flow-of-the-mints/application-configuration.md)
  * [Default Configuration](Default Configuration)
  * [Palette Configuration](palette-configuration.md)
* [Application Configuration](application-configuration.md)

# WIKI Content
*  MINTS
	*  Administering-MINTS
		* [Backing-Up-Resources](Backing-Up-Resources.md)
		* [Managing-Resources](Managing-Resources.md)
		* [Managing-Servers](Managing-Servers.md)
		* [Viewing-Admin-Logs](Viewing-Admin-Logs.md)
		* [Viewing-Integrations-by-using-UI](Viewing-Integrations-by-using-UI.md)
	*  Configuring-MINTS
		* [Configuring-MINTS-Backend](Configuring-MINTS-Backend.md)
		* [Configuring-MINTS-to-run-in-a-Docker](Configuring-MINTS-to-run-in-a-Docker.md)
		* [Configuring-MINTS-to-run-on-premises](Configuring-MINTS-to-run-on-premises.md)
		* [Configuring-MINTS-UI](Configuring-MINTS-UI.md)
		* [Configuring-MINTS-UI](Configuring-MINTS-UI.md)
	*  Deploying-Integration-Solutions
		* [Adding-Instances](Adding-Instances.md)
		* [Deployment-Rules-and-Guidelines](Deployment-Rules-and-Guidelines.md)
	*  Developing-Integration-Solutions
		* [Developing-Integrations-by-using-Patterns](Developing-Integrations-by-using-Patterns.md)
		* [Developing-Integration-Services](Developing-Integration-Services.md)
		* [Developing-Maps-using-MTX](Developing-Maps-using-MTX.md)
		* [Developing-Patterns](Developing-Patterns.md)
		* [Developing-User-Defined-Extensions](Developing-User-Defined-Extensions.md)
	*  Installing-and-Uninstalling-MINTS
		* [Installing-and-Uninstalling-Complimentary-Product]
			(Installing-and-Uninstalling-Complimentary-Products.md)        
		* [Installing-MINTS](Installing-MINTS.md)
		* [Uninstalling-MINTS](Uninstalling-MINTS.md)
	*  Integration-Testing
		* [Automated-Testing](Automated-Testing.md)
		* [Developing-Test-Cases](Developing-Test-Cases.md)
		* [Integrating-Test-Cases-in-Pipeline](Integrating-Test-Cases-in-Pipeline.md)
	*  Performance
		* [Analyzing-Resource-Planning](Analyzing-Resource-Planning.md)
		* [Performance-Planning](Performance-Planning.md)
		* [Tuning-Resources](Tuning-Resources.md)
	*  Planning-MINTS-Solution
		* [KYD](KYD.md)
		* [License-Requirements](License-Requirements.md)
		* [Operation-Modes](Operation-Modes.md)
	*  Product-overview
		*  Technical-Overview
			* [Architecture](Architecture.md)
			* [Components](Components.md)
			* [Features](Features.md)
			* [Infrastructure](Infrastructure.md)
			* [MINTS-API](MINTS-API.md)
			* [MINTS-User-Interface](MINTS-User-Interface.md)
		* [FAQ](FAQ.md)
		* [Introduction](Introduction.md)
	*  Reference
		* [Configuration-and-Administration](Configuration-and-Administration.md)
		* [Integration-Development](Integration-Development.md)
		* [Security-Requirements](Security-Requirements.md)
		* [Troubleshooting](Troubleshooting.md)
	*  Security
		* [Security-for-MINTS-UI](Security-for-MINTS-UI.md)
		* [Security-for-runtime-components](Security-for-runtime-components.md)


